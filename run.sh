pandoc \
    --pdf-engine=xelatex \
    -V mainfont="CMU Serif" \
    -V monofont="CMU Typewriter Text" \
    -V linkcolor:blue \
    -V geometry:a4paper \
    -V geometry:margin=1.5cm \
    -V pagestyle=empty \
    project.md \
    -o project.pdf

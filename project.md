<!--Расскажите более подробно о каком-нибудь из своих проектов. Что хотелось сделать? Какие нетривиальные технические решения вы использовали? В чём были трудности и как вы их преодолевали? Пожалуйста, сохраните свой ответ в файле PDF, выложите его на Яндекс.Диск и дайте ссылку. Если у вас уже есть статья на эту тему и вы давали на неё ссылку в предыдущем вопросе, то можете поставить здесь прочерк.
-->
**ouinet.**

- Сеттинг: распределённое именование. Есть P2P content addressable storage, где блобы -- снэпшоты сайтов. Хочется сделать так, чтобы пользователь, запрашивая URL, получал его последнюю из видимых версию. Resolution должно быть сложно обнаружить в трафике и заблокировать. Протокол именования также должен быть сериализуем, чтобы такой "/etc/hosts" можно было принести на флешке.
- Принципиально возможные дизайны: один резолвер, иерархия резолверов, "pull"-волна (спросить соседей), "push"-волна, то же на определённой топологии, суперноды, DHT.
- Сразу отрежем кусочек пространства: нам нужно Availability-Partioning (CAP) => мы выбираем последнюю версию в данном кусочке сети или 404; нам нужно secure, memorable (Zooko's triangle) => запись в сеть осуществляется только заранее выбранным набором ключей.
- Начнём с простого: одного резолвера. Им будет та же нода, что сохраняет снэпшоты. Чтобы это было устойчивым к цензуре, будем подключаться к ней как к onion service или через I2P. Это не удовлетворяет A, но работает и позволяет отлаживать сериализуемый протокол. Побочно, это позволяет сравнить нашу реализацию с портами Tor и I2P на Android.
- Мы не можем сделать много одинаковых резолверов, доступных по Tor/I2P, потому что система должна работать в регионах, где выключили транзит трафика за пределы региона. Такие регионы могут появляться непредсказуемо, значит, система должна самоконфигурироваться.
- Попробуем готовые решения:
	- Mainnet IPNS загружен так, что resolution занимает до 10 минут. Сделать свой net не подходит, потому что wire format IPNS заметен на DPI. Кроме того, демон нужно патчить, чтобы он не использовал весь compute, особенно на телефоне.
	- В GNS идёт большой рефакторинг транспортов, из-за которого (и до которого) ничего не работает. Также нет порта для Android.
	- Onion services спроектированы для нас неправильно: они sec-glob, и реализация требует мультихоп-маршрутизации.
- Остаётся делать своё. CAS уже реализован на bittorrent, поэтому натурально переиспользовать DHT. Для этого есть стандарты BEP5/BEP44. У системы много возможных рабочих условий, поэтому есть [testbed](https://github.com/equalitie/EchoChamber).
- Результат на <https://github.com/equalitie/ouinet>, бэкграунд про другие интересные задачи [рядом](https://github.com/equalitie/ouinet/blob/technical-whitepaper/doc/ouinet-network-whitepaper.md).

**proxyrot.**

- Сеттинг: OSINT. Есть длинный список SSH-хостов и паролей, и краулеры. Хочется, чтобы трафик краулеров проксировался через хосты так, чтобы они не знали логины и пароли. Не все хосты онлайн, не все хосты стабильны. Одно решение - на одном сервере иметь список, устанавливать туннели, тестировать и выдерживать на стабильность, и выдавать краулеру сокет, трафик которого будет прозрачно проксироваться через хост.
- Коллега набросал решение на `subprocess.call("ssh -D{}")`; ожидаемо, оно не масштабировалось больше чем на несколько тысяч туннелей из-за contention. Я начал работать над альтернативным решением с green threads и libssh.
-Тогда у меня перед глазами был [thrussh](https://nest.pijul.com/pijul_org/thrussh), и я попробовал использовать его. `./demo user@localhost` работало без проблем, но на реальном списке выявились проблемы: SSH поддерживает много шифров, а thrussh - только модные (не-legacy). Я пропатчил его для поддержки режимов RSA, но при виде DES стало понятно, что так продолжать нельзя.
- Я нашёл на Hackage довольно полные биндинги к libssh2 и пропатчил их для поддержки port forwarding. Каждый туннель "держится" своим green thread, который кладёт handle в конкурентную очередь.
- Результат выглядел так: сервер держит пул поднятых туннелей размера n, циклично проходя по списку. Краулер делает `GET /get-port` и получает TCP-порт, трафик в который проксируется (аутентификация не была нужна). Туннель закрывается, когда краулер отключается от порта.

**profilum.**

- Сеттинг: анализ данных. Есть freeform-тексты вакансий, из которых экстрагированы навыки. Хочется эмбеддинг такой, чтобы "опытный пользователь ПК" и "владение персональным компьютером" были рядом; иными словами, чтобы у навыков появилась каноническая форма.
- Off-the-shelf word2vec на n-грамах => tf-idf => dimensionality reduction не сработал.
- Предположили, что word2vec можно оставить как есть (во всяком случае, было неясно, где взять релевантные данные, чтобы его засидить), и основной вопрос в dimensionality reduction / кластеризации.
- Попробовали: батч-кластеризатор с убывающей threshold. Рассмотрим скиллы как граф без рёбер, будем соединять каждый `cos(все скиллы, батч_i скиллов) > threshold_i`. Сработало, но не масштабировалось.
- Попробовали: сначала то же с частотными, потом с остальными многопоточно. Лучше, но медленно.
- Попробовали: убрать граф (идеи о метриках связности не пригодились) и переписать на numpy. Сработало.
- Выбрали частотный член кластера как label.
Так у навыков появилась каноническая форма.

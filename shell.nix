{ nixpkgs ? import <nixpkgs> {} }:
with nixpkgs.pkgs;
stdenv.mkDerivation {
  name = "textest";
  src = ./.;
  buildInputs = [
    (texlive.combine {
      inherit (texlive) scheme-basic xetex fontspec euenc collection-latexrecommended collection-langcyrillic;
    })
    pandoc
  ];

  FONTCONFIG_FILE = makeFontsConf { fontDirectories = [ cm_unicode ]; };
}
